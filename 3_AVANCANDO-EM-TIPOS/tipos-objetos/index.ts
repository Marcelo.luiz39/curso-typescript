function passCoordinates({ coord }: { coord: { x: number; y: number}  }): void {
  console.log(`X coordenada: ${coord.x}, Y coordenada: ${coord.y}`)
}

const objCoordinates = { x: 329, y: 84.3 }

passCoordinates({ coord: objCoordinates })

const personObj: { name: string, age: number } = { name: 'John', age: 30 }

console.log(personObj)
