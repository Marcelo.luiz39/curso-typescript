/**
 * Literal types
 * https://www.typescriptlang.org/docs/handbook/literal-types.html
 * Literal types é um recurso que permite colocar valores como tipos.
 * Isso restringe o uso a nao so tipos, como também os próprios valores.
 * Este recurso e muito utilizado com union types.
 */

let test: 'testando'

// teste = 1  error TS2322: Type '1' is not assignable to type '"testando"'.
test = 'testando'
console.log(test)

type Coordinate = '123' | '456' | '789' | '000' | 'ABC'

const showDirectory = (direction: 'left' | 'right' | 'center') => {
  console.log(`A direção é: ${direction}`)
}

showDirectory('left')
showDirectory('right')

const showCoordinate = (coordinate: Coordinate) => {
  console.log(`A coordenada é: ${coordinate}`)
}

showCoordinate('123')
showCoordinate('ABC')
