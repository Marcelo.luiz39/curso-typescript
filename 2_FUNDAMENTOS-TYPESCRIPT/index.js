// 1 - Numbers
var number1 = 10;
console.log(number1);
number1 = 44;
console.log(number1);
console.log(typeof number1);
var y = 3.14329492349;
console.log(y.toPrecision(3));
// 2 - string
var firstName = 'Marcelo Luiz';
console.log(firstName.toUpperCase());
var lastName = 'Souza';
var fullName;
fullName = "".concat(firstName, " ").concat(lastName);
console.log(fullName);
// 3 - boolean
var a = true;
console.log(typeof a);
a = false;
// 4 - inference e annotation
var ann = 'Teste';
var inf = 'Testando';
// 5 - compile automático
var q = 10;
console.log(q);
q = 11;
console.log(q);
// 6 - desafio 2
var n1 = 10;
var numberToString = n1.toString();
var printMyNumber = "Eu quero imprimir o n\u00FAmero ".concat(numberToString);
console.log(printMyNumber);
