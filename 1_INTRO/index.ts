const firstName = 'Marcelo Luiz'
const anotherName = 1234
const x = true

function greeting(name: string) {
  console.log("ola " + name)
}

greeting(firstName)
// greeting(anotherName)
// greeting(x)