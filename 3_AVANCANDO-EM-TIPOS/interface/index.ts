interface Point {
  x: number
  y: number
  z?: number
}

const showCoord = (point: Point) => {
  if(point.z){
  return console.log(`X: ${point.x}, Y: ${point.y}, Z: ${point.z}`)
  }
  return console.log(`X: ${point.x}, Y: ${point.y}`)
}

showCoord({x: 12343, y: 25678})
showCoord({x: 1, y: 2, z: 3})

const point: Point = {x: 100, y: 200}
const point2: Point = {x: 1, y: 2, z: 3}

showCoord(point)
console.log(point2)