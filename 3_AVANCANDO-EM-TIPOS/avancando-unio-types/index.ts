const showRole = (role: string | boolean) => {
  if(typeof role === 'boolean') {
    return console.log("Usuário nao autorizado !")
  } 
  return console.log(`A função do usuário é: ${role}`)
}

showRole(false)
showRole('Admin')
showRole(true)