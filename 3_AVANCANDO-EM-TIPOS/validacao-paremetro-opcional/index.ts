function advancedGreetings({
  person,
}: {
  person: {
    name: string
    age: number
    hobbies?: string[]
    role: string
  }
}) {
  if (person.hobbies !== undefined) {
    return `Olá, meu nome é ${person.name}, tenho ${person.age} anos, tenho o hobby de ${person.hobbies} e sou ${person.role}.`
  }
  return `Olá, meu nome é ${person.name}, tenho ${person.age} anos, e sou ${person.role}.`
}

console.log(
  advancedGreetings({
    person: {
      name: 'Marcelo Luiz',
      age: 44,
      hobbies: ['Jogar'],
      role: 'Desenvolvedor de software',
    },
  })
)

console.log(
  advancedGreetings({
    person: {
      name: 'Julia Souza',
      age: 19,
      role: 'Aux. de Enfermagem',
    },
  })
)
