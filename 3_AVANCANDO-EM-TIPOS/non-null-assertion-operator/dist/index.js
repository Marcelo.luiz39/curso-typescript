"use strict";
/**
 * NON-NULL ASSERTION OPERATOR
 * As vezes o typescript não consegue inferir o tipo de uma variável, baseado em um valor que no momento do código ainda não esta disponível;
 * Porem sabemos que o valor que estamos tentando acessar sera preenchido, podemos evitar o erro.
 * O operador de negação (!) é usado para fazer isso
 */
const doc = document;
const p = doc.getElementById('some-paragraph');
console.log(p.innerText);
