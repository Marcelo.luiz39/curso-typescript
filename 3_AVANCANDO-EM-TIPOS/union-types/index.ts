const showBalance = (balance: string | number) => {
  console.log(`O saldo da conta é ${balance}`)
}

showBalance('200')
// showBalance(true) nao funciona pois so aceita string ou number

const arr2: Array<string | number | boolean> = ['teste', 1, true]

console.log(arr2)
