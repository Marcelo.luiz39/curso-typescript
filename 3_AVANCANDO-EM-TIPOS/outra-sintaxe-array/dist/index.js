"use strict";
// 1. Arrays
let numbers = [1, 2, 3];
console.log(numbers);
numbers.push(400);
console.log(numbers);
console.log(numbers.length);
console.log(numbers[5]);
const nomes = ['Marcelo', 'Julia', 'Kaique'];
console.log(nomes);
