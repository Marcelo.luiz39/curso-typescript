/**
 * Symbol
 * De forma resumida, o symbol cria uma referência unica a um valor.
 * Ou seja, mesmo se o valor for alterado, o symbol não será alterado.
 * Assim mesmo que ele possua o mesmo valor de outra variável, teremos valores sendo considerados diferentes
 */

let symbolA: symbol = Symbol('a') // Definindo o tipo para a variável usa-se o symbol com s minuscula
let symbolB = Symbol('a') // Com inferência de tipo usa-se o Symbol com S maiúscula

console.log(symbolA == symbolB)
console.log(symbolA === symbolB)


const a = Symbol('abc')
const b = Symbol('abc')

console.log(typeof a === typeof b) // com const usa-se o typeof