// 1. Arrays
let numbers: number[] = [1, 2, 3]

console.log(numbers)

numbers.push(4)

console.log(numbers)
console.log(numbers.length)
console.log(numbers[2])

const nomes: string[] = ['Marcelo', 'Julia', 'Kaique']

console.log(nomes)