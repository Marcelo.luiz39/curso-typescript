function passCoordinates(_a) {
    var coord = _a.coord;
    console.log("X coordenada: ".concat(coord.x, ", Y coordenada: ").concat(coord.y));
}
var objCoordinates = { x: 329, y: 84.3 };
passCoordinates({ coord: objCoordinates });
var personObj = { name: 'John', age: 30 };
console.log(personObj);
