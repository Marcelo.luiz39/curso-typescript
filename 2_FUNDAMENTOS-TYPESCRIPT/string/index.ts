// 2 - string
const firstName: string = 'Marcelo Luiz'

console.log(firstName.toUpperCase())

const lastName: string = 'Souza'

let fullName: string

fullName = `${firstName} ${lastName}`

console.log(fullName)
