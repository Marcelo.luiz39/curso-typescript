function showNumbers(a: number, b: number, c?: number) {
  console.log(`A: ${a}`)
  console.log(`B: ${b}`)
  if (c) console.log(`C: ${c}`) // aqui usamos if para que nao nos dê undefined
} // aqui no elemento c usamos o ? para que seja opcional

showNumbers(1, 2, 3)
showNumbers(1, 2)
