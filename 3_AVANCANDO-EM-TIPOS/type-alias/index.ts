const showId = (id: number | string) => {
  return console.log(`O id é ${id}`)
}

showId(44)
showId('awr-123-esd-456')

type ID = number | string

const showId2 = (id: ID) => {
  return console.log(`O id com type alias é: ${id}`)
}

console.log('------ Com Type Alias ------')

showId2(346)
showId2('wey4jsn-brn=fn')