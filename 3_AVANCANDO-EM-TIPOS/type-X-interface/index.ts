// Com interface podemos passar mais de uma informação

interface Person {
  name: string
}

interface Person {
  age: number
}

const somePerson: Person = {
  name: 'Marcelo',
  age: 44
}

console.log(somePerson) // desta forma com interface podemos adicionar mais informação

// Com Type alias nao podemos adicionar mais informação

/* type PersonType = {
  name: string
}

type PersonType = {
  age: number
} 
aqui vai dar erro index.ts:24:6 - error TS2300: Duplicate identifier 'PersonType'.
*/

// Ou seja quando queremos adicionar mais informação podemos usar interface quando nao precisamos adicionar mais informação usamos type alias
