let x: number = 10
console.log(x)
console.log(typeof x)

x = 44
console.log(x)

// num = 'Teste'  ** aqui da erro de compilação por ser uma string **

const pi: number = 3.14329492349

console.log(typeof pi)
console.log(pi)