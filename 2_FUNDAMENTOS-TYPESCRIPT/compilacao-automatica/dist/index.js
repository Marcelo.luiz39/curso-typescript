"use strict";
// 1 - Numbers
let number1 = 10;
console.log(number1);
number1 = 44;
console.log(number1);
console.log(typeof number1);
const y = 3.14329492349;
console.log(y.toPrecision(3));
// 2 - string
const firstName = 'Marcelo Luiz';
console.log(firstName.toUpperCase());
const lastName = 'Pereira de Souza';
let fullName;
fullName = `${firstName} ${lastName}`;
console.log(fullName);
// 3 - boolean
let a = true;
console.log(typeof a);
a = false;
// 4 - inference e annotation
const ann = 'Teste';
const inf = 'Testando';
// 5 - compile automático
let q = 10;
console.log(q);
q = 11;
console.log(q);
// 6 - desafio 2
const n1 = 10;
const numberToString = n1.toString();
const printMyNumber = `Eu quero imprimir o número ${numberToString}`;
console.log(printMyNumber);
